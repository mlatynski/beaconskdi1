﻿using Microsoft.Owin.Hosting;
using System;

namespace BeaconsHubServer
{
    static class Program
    {
        static void Main(string[] args)
        {
            string url = "http://localhost:6789";
            using (WebApp.Start(url))
            {
                Console.WriteLine("Server running on {0}", url);
                Console.ReadLine();
            }
        }
    }
}
