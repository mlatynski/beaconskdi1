﻿using Commons.Model;
using Microsoft.AspNet.SignalR;
using System;
using System.Threading.Tasks;

namespace BeaconsHubServer
{
    public class BeaconHub : Hub
    {
        public override Task OnConnected()
        {
            Console.WriteLine("OnConnected");

            return base.OnConnected();
        }

        public async Task Send(BeaconHubMessage message)
        {
            await Clients.Others.Receive( message);
        }
    }
}
