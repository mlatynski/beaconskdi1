﻿namespace Commons.Model
{
    public class EndpointSignal
    {
        public int Id { get; set; }

        public double Rssi { get; set; }
    }
}
