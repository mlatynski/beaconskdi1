﻿using BeaconsServer.Model;
using System.Collections.Generic;

namespace BeaconsServer.Services
{
    public interface IEndpointsScannerService
    {
        List<RedisEndpoint> Get(List<RedisDevice> devices);
    }
}
