﻿using BeaconsServer.Model;
using Commons.Model;
using System;
using System.Collections.Generic;

namespace BeaconsServer.Services
{
    public interface ICollectorService
    {
        List<BeaconSignals> Add(IEnumerable<RedisResponse> data, TimeSpan expirationTime);
    }
}
