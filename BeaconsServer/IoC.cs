﻿using BeaconsServer.Services;
using Microsoft.Extensions.DependencyInjection;

namespace BeaconsServer
{
    public class IoC
    {
        private readonly ServiceProvider _serviceProvider;

        public IoC(string hubUrl)
        {
            _serviceProvider = new ServiceCollection()
               .AddScoped<IApiService, ApiService>()
               .AddScoped<IEndpointsScannerService, EndpointsScannerService>()
               .AddScoped<IRedisService, RedisService>()
               .AddScoped<ICollectorService, CollectorService>()
               .AddScoped<IBeaconHubService>(_ => new BeaconHubService($"http://{hubUrl.Trim()}:6789/signalr"))
               .AddScoped<IBeaconServer, BeaconServer>()
               .BuildServiceProvider();
        }

        public T GetService<T>()
        {
            return _serviceProvider.GetService<T>();
        }
    }
}
