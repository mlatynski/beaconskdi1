﻿namespace BeaconsServer.Model
{
    public class RedisDevice
    {
        public int Id { get; set; }

        public string Mac { get; set; }

        public int Port { get; set; }

        public int Database { get; set; }

        public string Key { get; set; }

        public double PositionX { get; set; }

        public double PositionY { get; set; }
    }
}
