﻿using BeaconsServer.Tools;
using Commons.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BeaconsServer.Model
{
    public class EndpointFilterMap
    {
        private readonly IDictionary<int, FilterTimestamp> _dictionary;


        public EndpointFilterMap()
        {
            _dictionary = new Dictionary<int, FilterTimestamp>();
        }

        public int Count => _dictionary.Count;

        public FilterTimestamp AddAndGet(int id, double rssi, DateTime date)
        {
            FilterTimestamp item = null;

            if (!_dictionary.ContainsKey(id))
            {
                item = new FilterTimestamp
                {
                    Filter = new KalmanFilter(rssi),
                    Date = date
                };

                _dictionary.Add(id, item);
            }
            else
            {
                item = _dictionary[id];
                item.Date = date;
            }

            return item;
        }

        public void Clean(int id, DateTime expirationDate)
        {
            if (_dictionary.ContainsKey(id))
            {
                var item = _dictionary[id];

                if (item.Date < expirationDate)
                {
                    _dictionary.Remove(id);
                }
            }
        }

        public List<EndpointSignal> GetAll()
        {
            return _dictionary.Select(x => new EndpointSignal
            {
                Id = x.Key,
                Rssi = x.Value.Filter.GetResult()
            })
            .ToList();
        }
    }
}
