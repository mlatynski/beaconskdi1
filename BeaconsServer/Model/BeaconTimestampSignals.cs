﻿using System.Collections.Generic;

namespace BeaconsServer.Model
{
    public class BeaconTimestampSignals
    {
        public string Mac { get; set; }

        public List<EndpointTimestampSignals> EndpointSignals { get; set; }
    }
}
