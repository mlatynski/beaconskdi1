﻿using BeaconsServer.Tools;
using System;

namespace BeaconsServer.Model
{
    public class FilterTimestamp
    {
        public KalmanFilter Filter { get; set; }

        public DateTime Date { get; set; }
    }
}
