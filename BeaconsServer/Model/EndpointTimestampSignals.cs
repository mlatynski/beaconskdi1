﻿using System.Collections.Generic;

namespace BeaconsServer.Model
{
    public class EndpointTimestampSignals
    {
        public int Id { get; set; }

        public List<RssiTimestamp> Signals { get; set; }
    }
}
