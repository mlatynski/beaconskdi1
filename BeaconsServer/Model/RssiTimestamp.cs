﻿using System;

namespace BeaconsServer.Model
{
    public class RssiTimestamp
    {
        public double Rssi { get; set; }

        public DateTime Date { get; set; }
    }
}
