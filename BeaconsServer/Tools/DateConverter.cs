﻿using System;

namespace BeaconsServer.Tools
{
    public static class DateConverter
    {
        private static DateTime unixEpochDate = new DateTime(1970, 1, 1);


        public static DateTime FromUnix(double ticks)
        {
            return unixEpochDate + TimeSpan.FromSeconds(ticks);
        }

        public static DateTime TrimToSeconds(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second);
        }
    }
}
