﻿using BeaconsWpf.Dialogs;
using BeaconsWpf.Model;
using BeaconsWpf.Services;
using BeaconsWpf.Tools;
using Commons.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace BeaconsWpf.ViewModel
{
    public class MainViewModel : BaseViewModel
    {
        public string SelectedMac { get; set; }

        public Beacon SelectedBeacon { get; set; }

        public bool ShowAll { get; set; }

        public ObservableCollection<string> Macs { get; } = new ObservableCollection<string>();

        public ObservableCollection<Beacon> Beacons { get; } = new ObservableCollection<Beacon>();


        public ICommand AddCommand { get; set; }

        public ICommand RemoveCommand { get; set; }

        public ICommand NewCommand { get; set; }

        public ICommand ClearCommand { get; set; }

        public BeaconHubService HubService;


        public MainViewModel()
        {
            var dialog = new InputDialog();
            dialog.Text = "Server:";
            dialog.ResponseText = "localhost";

            if (dialog.ShowDialog() == true && !string.IsNullOrEmpty(dialog.ResponseText))
            {
                InitMacs();
                InitService(dialog.ResponseText);
                InitCommands();
            }
        }

        private void InitMacs()
        {
            Macs.Add("6C:61:D3:9C:9C:2C");
            Macs.Add("6C:61:D3:9F:A2:2C");
            Macs.Add("6C:61:D3:A4:4D:2B");
            Macs.Add("6C:61:D3:A7:31:2A");
            Macs.Add("*");

            ShowAll = false;
            SelectedMac = Macs.First();

            OnNotifyPropertyChanged(nameof(Macs));
        }

        private void InitService(string ip)
        {
            HubService = new BeaconHubService($"http://{ip.Trim()}:6789/signalr");

            HubService.OnBeaconsReceived += beacons =>
            {
                List<BeaconSignals> filteredBeacons = null;

                if (ShowAll)
                {
                    filteredBeacons = beacons;
                    ShowAll = false;
                }
                else
                {
                    var filteredMacs = Beacons.Select(x => x.Mac).ToList();
                    filteredBeacons = beacons.Where(x => filteredMacs.Contains(x.Mac)).ToList();
                }

                if (filteredBeacons.Any())
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        foreach (var b in filteredBeacons)
                        {
                            int rssi = b.EndpointSignals.Any() ? (int)Math.Round(b.EndpointSignals.First().Rssi) : -100;
                            var beacon = Beacons.Where(x => string.Compare(x.Mac.ToUpperInvariant(), b.Mac.ToUpperInvariant()) == 0).FirstOrDefault();

                            int bRssi = rssi;
                            double dDistance = CalculateDistance(rssi, -60.0);
                            string dDescription = rssi <= -90 ? "weak" : rssi <= -70 ? "average" : "strong";

                            if (beacon != null)
                            {
                                beacon.Rssi = bRssi;
                                beacon.Distance = dDistance;
                                beacon.Description = dDescription;
                            }
                            else
                            {
                                Beacons.Add(new Beacon
                                {
                                    Mac = b.Mac,
                                    Rssi = bRssi,
                                    Distance = dDistance,
                                    Description = dDescription
                                });

                                OnNotifyPropertyChanged(nameof(Beacons));
                            }
                        }
                    });
                }
            };

            HubService.Connect().ContinueWith(t =>
            {
                if (t.IsFaulted)
                {
                    MessageBox.Show("Nie można połączyć z hub");
                }
            });

        }

        private void InitCommands()
        {
            AddCommand = new RelayCommand(_ =>
            {
                if (string.Compare(SelectedMac, "*") == 0)
                {
                    ShowAll = true;
                }
                else if (!Beacons.Any(x => string.CompareOrdinal(x.Mac, SelectedMac) == 0))
                {
                    Beacons.Add(new Beacon
                    {
                        Mac = SelectedMac
                    });

                    OnNotifyPropertyChanged(nameof(Beacons));
                }
            }, _ => !string.IsNullOrEmpty(SelectedMac));

            RemoveCommand = new RelayCommand(_ =>
            {
                if (SelectedBeacon != null)
                {
                    Beacons.Remove(SelectedBeacon);
                    SelectedBeacon = null;

                    OnNotifyPropertyChanged(nameof(Beacons));
                }
            }, _ => SelectedBeacon != null);

            NewCommand = new RelayCommand(_ =>
            {
                var dialog = new InputDialog();
                dialog.Text = "Mac:";

                if (dialog.ShowDialog() == true && !string.IsNullOrEmpty(dialog.ResponseText))
                {
                    Beacons.Add(new Beacon
                    {
                        Mac = dialog.ResponseText.ToUpperInvariant()
                    });

                    OnNotifyPropertyChanged(nameof(Beacons));
                }
            });

            ClearCommand = new RelayCommand(_ =>
            {
                ShowAll = false;
                Beacons.Clear();
            });
        }


        private double CalculateDistance(double rssi, double tx)
        {
            double rssiRatio = rssi / tx;

            if (rssiRatio < 1.0)
            {
                return Math.Sqrt(Math.Pow(rssiRatio, 10));
            }

            return Math.Abs(37.95 * Math.Pow(rssiRatio, 0.43) - 40.15);
        }
    }
}
