﻿using System.ComponentModel;

namespace BeaconsWpf.Model
{
    public class Beacon : INotifyPropertyChanged
    {
        private string _mac;

        private double _distance = double.NaN;

        private int _rssi = -100;

        private string _description;


        public string Mac
        {
            get
            {
                return _mac;
            }

            set
            {
                _mac = value;
                NotifyPropertyChanged(nameof(Mac));
            }
        }

        public double Distance
        {
            get
            {
                return _distance;
            }

            set
            {
                _distance = value;
                NotifyPropertyChanged(nameof(Distance));
            }
        }

        public int Rssi
        {
            get
            {
                return _rssi;
            }

            set
            {
                _rssi = value;
                NotifyPropertyChanged(nameof(Rssi));
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }

            set
            {
                _description = value;
                NotifyPropertyChanged(nameof(Description));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));

        }
    }
}
